# Schema-Org Extension for TYPO3

This extension is very easy to use. Simply install and include the delivered static template.


**Features:**

*   Easy to use
*   Simply install and link in the provided static template
*   Configure schema data in Setup-TypoScript
*   Add social media profiles
*   Configure different opening hours on different days
*   Insert video schema data for YouTube, Vimeo, etc

If you need some additional or custom feature - get in contact!


**Links:**

*   [Schema-Org Documentation](https://www.coding.ms/documentation/typo3-schema-org "Schema-Org Documentation")
*   [Schema-Org Bug-Tracker](https://gitlab.com/codingms/typo3-public/schema_org/-/issues "Schema-Org Bug-Tracker")
*   [Schema-Org Repository](https://gitlab.com/codingms/typo3-public/schema_org "Schema-Org Repository")
*   [TYPO3 Schema.org Productdetails](https://www.coding.ms/typo3-extensions/typo3-schemaorg "TYPO3 Schema.org Productdetails")
