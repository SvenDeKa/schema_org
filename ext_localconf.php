<?php
if (!defined('TYPO3')) {
    die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'SchemaOrg',
    'Schema',
    [\CodingMs\SchemaOrg\Controller\SchemaController::class => 'inject'],
    []
);
