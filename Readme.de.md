# Schema-Org Erweiterung für TYPO3

Diese Erweiterung für TYPO3 ist einfach zu verwenden - einfach installieren und das mitgelieferte Static-Template einbinden.


**Features:**

*   Einfach in der Handhabung
*   Einfach das mitgelieferte static Template installieren und einbinden
*   Konfiguriere die Schemadaten mithilfe von TypoScript
*   Socialmedia-Profile hinzufügen
*   Konfiguriere Unterschiedliche Öffnungszeiten an Wochentagen
*   Video Schema-Daten für YouTube, Vimeo, und weitere eintragen

Wenn ein zusätzliches oder individuelles Feature benötigt wird - kontaktiere uns gern!


**Links:**

*   [Schema-Org Dokumentation](https://www.coding.ms/documentation/typo3-schema-org "Schema-Org Dokumentation")
*   [Schema-Org Bug-Tracker](https://gitlab.com/codingms/typo3-public/schema_org/-/issues "Schema-Org Bug-Tracker")
*   [Schema-Org Repository](https://gitlab.com/codingms/typo3-public/schema_org "Schema-Org Repository")
*   [TYPO3 Schema.org Produktdetails](https://www.coding.ms/typo3-extensions/typo3-schemaorg "TYPO3 Schema.org Produktdetails")
