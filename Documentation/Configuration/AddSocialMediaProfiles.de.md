# Socialmedia-Profile hinzufügen

Links zu Socialmedia-Profilen oder anderen Seiten können wie folgt hinzugefügt werden:

```typo3_typoscript
plugin.tx_schemaorg {
	settings {
		page {
			#
			#...
			#
			sameAs {
				# Simply add multiple pages divided by comma
				ARRAY = https://twitter.com/codingms,https://www.instagram.com/codingms/
			}
			#
			#...
			#
		}
	}
}
```
