# How to add social media profiles

Links for some social media profiles or pages can be added like this:

```typo3_typoscript
plugin.tx_schemaorg {
	settings {
		page {
			#
			#...
			#
			sameAs {
				# Simply add multiple pages divided by comma
				ARRAY = https://twitter.com/codingms,https://www.instagram.com/codingms/
			}
			#
			#...
			#
		}
	}
}
```
